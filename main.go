package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

const form_html string =
`<!DOCTYPE html>
<html>
<head>
	<title>Upload!</title>
</head>
<body>
	<form method="post" enctype="multipart/form-data">
		<input type="file" id="file" name="file"><br />
		<input type="submit" value="Submit">
	</form>
</body>
</html>`

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if(r.Method == "GET") {
			if  _, err := io.WriteString(w, form_html); err  != nil { log.Fatal(err) }
		} else if(r.Method == "PUT" || r.Method == "POST") {
			hfile, header, err := r.FormFile("file")
			if err != nil { log.Fatal(err) }

			file, err := os.Create(header.Filename)
			if err != nil { log.Fatal(err) }

			_, err = io.Copy(file, hfile)
			if err != nil { log.Fatal(err) }

			log.Printf("File %s uploaded.", header.Filename)
		} else {
			http.NotFound(w, r)
		}
	})

	port := os.Getenv("HTTP_PORT")
	if len(port) == 0 { port = "8888" }

	log.Printf("Listening on port %s.", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
